(function ($) {

  Drupal.behaviors.webrtc_api_mediastream = {
    attach: function (context, settings) {

      window.URL = window.URL || window.webkitURL;
      navigator.getUserMedia  = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;
      video = document.querySelector(".webrtc_api_mediastream_video");
      localMediaStream = null;

      // Feature detection
      if (!navigator.getUserMedia) {
        alert('Your browser doesn\'t support mediastream.');
        return;
      }

      // Fetch the camera input.
      navigator.getUserMedia({video: true, audio: true},
        // Success
        function (stream) {
          video.src = window.URL.createObjectURL(stream);
          localMediaStream = stream;
        },
        // Error
        function (error) {
          alert('whoops something went wrong');
        }
      );
    }
  };

})(jQuery);
